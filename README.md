# CodeIgniter 4 Docker Boilerplate

## Defaults

| Services | Host      | Port | Version             |
| -------- | --------- | ---- | ------------------- |
| nginx    | localhost | 8081 | stable-alpine       |
| mariadb  | localhost | 3307 | latest              |
| php-fpm  | localhost | 9001 | 7.2-fpm-alpine      |
| composer | -         | -    | latest              |
| npm      | -         | -    | node:12.18.1-alpine |

## Setup

1. Run from the host machine:

```powershell
$ cd src
$ rm -rf README.md public/
$ composer create-project codeigniter4/appstarter .
```

2. Rename `env` to `.env`
3. Setup the database connection

```
database.default.hostname = mariadb
database.default.database = app
database.default.username = app
database.default.password = appsecret
database.default.DBDriver = MySQLi
```

> Note that the hostname is mariadb, that's because the network name defined in the docker-composer.yml file

4. Run from the host machine:

```powershell
$ cd ..
$ docker-composer up -d --build
```

Docker will create all the container and detach. If all of the containers were created successfully, you can see your app on `http://localhost:8081/`

## Tools

### Database

You can connect to the database using your system's mysql cli:

Example:

```powershell
$ mysql -h localhost -P 3307 -u app -p app
```

> Note that we're specifying the hostname and the port

### Composer

You can run composer through docker-compose using this command:

```powershell
$ docker-compose run composer --version
```

### npm

You can run npm through docker-compose using this command:

```powershell
$ docker-compose run npm -v
```

### CodeIgniter 4 - Spark

Once you've setup your codeigniter4 project, you can run `spark` through docker-compose using this command:

```powershell
$ docker-compose run spark
```

## Additional Information

You can setup Apache's VirtualHost and use proxy pass using this block:

```
<VirtualHost *:80>
    ServerName codeigniter4.local

    ProxyPreserveHost On

    ProxyPass / http://0.0.0.0:8081/
    ProxyPassReverse / http://0.0.0.0:8081/
</VirtualHost>
```

So when you access `http://codeigniter.local/`, it will point to the docker container `localhost:8081`
